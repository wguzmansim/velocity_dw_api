﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1
{
    public class Status
    {
        public bool Success { get; internal set; }
        public string Error { get; internal set; }

        public Status(bool Success)
        {
            this.Success = Success;
            if (!Success)
            {
                Error = "Unknown error";
            }
        }

        public Status(string ErrorMessage)
        {
            this.Success = false;
            this.Error = ErrorMessage;
        }

        public static Status NoToken()
        {
            return new Status("A valid Token is required.");
        }
    }
}