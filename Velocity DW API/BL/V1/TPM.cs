﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Velocity_DW_API.DAL;
using Velocity_DW_API.Models.V1;
using Velocity_DW_API.Models.V1.DataQuery;

namespace Velocity_DW_API.BL.V1
{
    public class TPM
    {
        public MetricsResponse GetMetrics(MetricsRequest Req, string Token)
        {
            var response = new MetricsResponse();
            TokenResponse _token;

            try
            {
                _token = Utility.DoValidation(ConfigurationManager.AppSettings["TokenApiURL"], Token);
                if (!_token.Status.Success)
                {
                    throw new Exception(_token.Status.Error);
                }
            }
            catch (Exception ex)
            {
                return new MetricsResponse() { Status = new Status(ex.Message) };
            }

            try
            {
                response = new DAL.V1.TPM().GetTPMData(Req);
            }
            catch (Exception ex)
            {
                response.Status = new Status(ex.Message);
            }
            
            return response;
        }
    }
}