﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1
{
    public class Summary
    {
        public decimal Average { get; set; }
        public decimal Max { get; set; }
        public decimal Min { get; set; }
    }
}