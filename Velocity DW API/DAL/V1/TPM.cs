﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Web;

using Velocity_DW_API.Models.V1;
using Velocity_DW_API.Models.V1.DataQuery;

namespace Velocity_DW_API.DAL.V1
{
    public class TPM
    {
        public MetricsResponse GetTPMData(MetricsRequest Req)
        {
            var result = new MetricsResponse();

            var conn = new OdbcConnection(Utils.GetRedshiftConnectionString());
            var dtTPM = new DataTable();
            var dtPro = new DataTable();

            try
            {
                conn.Open();
                OdbcDataAdapter daPro = new OdbcDataAdapter(GetProfileDataQuery(Req), conn);
                OdbcDataAdapter daTPM = new OdbcDataAdapter(GetTPMDataQuery(Req), conn);
                daPro.Fill(dtPro);
                daTPM.Fill(dtTPM);
                result = MapToTPM(dtTPM, dtPro,Req);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }

            result.Status = new Status(true);

            return result;
        }

        private string GetProfileDataQuery(MetricsRequest req)
        {
            var sbQuery = new StringBuilder();

            sbQuery.AppendLine("SELECT");
            sbQuery.AppendLine("	P.pro_id,");
            sbQuery.AppendLine("	P.pro_name,");
            sbQuery.AppendLine("	P.pro_directory_name,");
            sbQuery.AppendLine("	P.pro_name pro_business_name,");
            sbQuery.AppendLine("	P.pro_address pro_address_line_1,");
            sbQuery.AppendLine("	P.pro_address_line2 pro_address_line_2,");
            sbQuery.AppendLine("	P.pro_city,");
            sbQuery.AppendLine("	P.pro_state,");
            sbQuery.AppendLine("	P.pro_zip_code,");
            sbQuery.AppendLine("	P.pro_phone,");
            sbQuery.AppendLine("	'' pro_toll_free_number,");
            sbQuery.AppendLine("	'' pro_profile_image,");
            sbQuery.AppendLine("	'' pro_directory_image	");
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine("	pro.pro_profile P");
            sbQuery.AppendLine("WHERE");
            sbQuery.AppendLine("	CAST(P.pro_id AS VARCHAR(256)) = '" + req.ProfileID + "'");
            sbQuery.AppendLine(";");

            return sbQuery.ToString();
        }

        private MetricsResponse MapToTPM(DataTable dtTPM, DataTable dtProfile, MetricsRequest req)
        {
            var result = new MetricsResponse();

            #region Profile Data
            if (dtProfile.Rows.Count > 0)
            {
                //we have profile data... good!
                result.Profile = new Profile()
                {
                    ID = dtProfile.Rows[0]["pro_id"].ToString(),
                    Name = dtProfile.Rows[0]["pro_name"].ToString(),
                    DirectoryName = dtProfile.Rows[0]["pro_directory_name"].ToString(),
                    BusinessName = dtProfile.Rows[0]["pro_business_name"].ToString(),
                    AddressLine1 = dtProfile.Rows[0]["pro_address_line_1"].ToString(),
                    AddressLine2 = dtProfile.Rows[0]["pro_address_line_2"].ToString(),
                    City = dtProfile.Rows[0]["pro_city"].ToString(),
                    State = dtProfile.Rows[0]["pro_state"].ToString(),
                    ZipCode = dtProfile.Rows[0]["pro_zip_code"].ToString(),
                    Phone = dtProfile.Rows[0]["pro_phone"].ToString(),
                    TollfreeNumber = dtProfile.Rows[0]["pro_toll_free_number"].ToString(),
                    ProfileImage = dtProfile.Rows[0]["pro_profile_image"].ToString(),
                    DirectoryImage = dtProfile.Rows[0]["pro_directory_image"].ToString()
                };
            }
            #endregion

            #region TPM Data
            if (dtTPM.Rows.Count > 0)
            {
                //we have metrics data... good!
                result.ThirdPartyMetrics = new List<ThirdPartyMetric>();

                var tpmt = new ThirdPartyMetric();
                var mt = new Metric();
                var currentMetric = string.Empty;

                for (int i = 0; i < dtTPM.Rows.Count; i++)
                {
                    if (tpmt.ThirdPartyProviderID == null || tpmt.ThirdPartyProviderID.ToLower() != dtTPM.Rows[i]["tpt_id"].ToString().ToLower())
                    {
                        //new TPM, set the properties
                        tpmt = new ThirdPartyMetric()
                        {
                            Aggregation = req.Aggregation,
                            StartDate = req.StartDate,
                            EndDate = req.EndDate,
                            ThirdPartyName = dtTPM.Rows[i]["tpt_name"].ToString(),
                            ThirdPartyProviderID = dtTPM.Rows[i]["tpt_id"].ToString(),
                            Metrics = new List<Models.V1.Metric>()
                        };

                        result.ThirdPartyMetrics.Add(tpmt);
                    }

                    if (dtTPM.Rows[i]["mdf_friendly_name"].ToString().ToLower() != currentMetric.ToLower())
                    {
                        //new Metric, set its properties
                        mt = new Metric()
                        {
                            MetricID = dtTPM.Rows[i]["mdf_id"].ToString(),
                            MetricName = dtTPM.Rows[i]["mdf_friendly_name"].ToString(),
                            MetricFriendlyName = dtTPM.Rows[i]["mdf_friendly_name"].ToString(),
                            MetricDescription = dtTPM.Rows[i]["mdf_description"].ToString(),
                            Summary = new Summary(),
                            MetricValues = new List<MetricValue>()
                        };
                        tpmt.Metrics.Add(mt);
                        currentMetric = dtTPM.Rows[i]["mdf_friendly_name"].ToString();
                    }

                    mt.MetricValues.Add(new MetricValue()
                    {
                        Date = Convert.ToDateTime(dtTPM.Rows[i]["date"]),
                        Value = decimal.Parse(dtTPM.Rows[i]["mtv_metric_value"].ToString())
                    });
                }

                //Calculate summary object for each group of metrics
                for (int i = 0; i < result.ThirdPartyMetrics.Count; i++)
                {
                    for (int j = 0; j < result.ThirdPartyMetrics[i].Metrics.Count; j++)
                    {
                        result.ThirdPartyMetrics[i].Metrics[j].Summary = new Summary()
                        {
                            Average = result.ThirdPartyMetrics[i].Metrics[j].MetricValues.Average(x => x.Value),
                            Max = result.ThirdPartyMetrics[i].Metrics[j].MetricValues.Max(x => x.Value),
                            Min = result.ThirdPartyMetrics[i].Metrics[j].MetricValues.Min(x => x.Value)
                        };
                    }
                }
            }
            #endregion

            return result;
        }

        private string GetTPMDataQuery(MetricsRequest req)
        {
            var sbQuery = new StringBuilder();

            sbQuery.AppendLine("SELECT");
            switch (req.Aggregation)
            {
                case AggregationLevel.Yearly:
                    sbQuery.AppendLine("	T.tim_year date,");
                    break;
                case AggregationLevel.Monthly:
                    sbQuery.AppendLine("	T.tim_month date,");
                    break;
                case AggregationLevel.Daily:
                    sbQuery.AppendLine("	T.tim_date date,");
                    break;
                case AggregationLevel.None:
                default:
                    sbQuery.AppendLine("	T.tim_date date,");
                    break;
            }
            sbQuery.AppendLine("	MV.mtv_pro_id,");
            sbQuery.AppendLine("	TP.tpt_id,");
            sbQuery.AppendLine("	TP.tpt_name,");
            sbQuery.AppendLine("	MAX(MD.mdf_id) mdf_id,");
            sbQuery.AppendLine("	MD.mdf_friendly_name,");
            sbQuery.AppendLine("	MAX(MD.mdf_description) mdf_description,");
            sbQuery.AppendLine("	CASE MAX(MD.mdf_aggregation_function)");
            sbQuery.AppendLine("		WHEN 'SUM' THEN SUM(MV.mtv_metric_value)");
            sbQuery.AppendLine("		WHEN 'AVG' THEN AVG(MV.mtv_metric_value)");
            sbQuery.AppendLine("		WHEN 'COUNT' THEN COUNT(MV.mtv_metric_value)");
            sbQuery.AppendLine("		WHEN 'MAX' THEN MAX(MV.mtv_metric_value)");
            sbQuery.AppendLine("		WHEN 'MIN' THEN MIN(MV.mtv_metric_value)");
            sbQuery.AppendLine("		WHEN 'MEDIAN' THEN MEDIAN(MV.mtv_metric_value)");
            sbQuery.AppendLine("		ELSE SUM(MV.mtv_metric_value)"); //Default behavior for new metrics
            sbQuery.AppendLine("	END mtv_metric_value");
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine("	tpm.mtv_metric_value MV");
            sbQuery.AppendLine("	JOIN tim.tim_time T ON MV.mtv_tim_date = T.tim_date");
            sbQuery.AppendLine("	JOIN tpm.mdf_metric_definition MD ON MD.mdf_id = MV.mtv_mdf_id");
            sbQuery.AppendLine("	JOIN tpm.tpt_third_party TP ON TP.tpt_id = MV.mtv_tpt_id");
            sbQuery.AppendLine("WHERE");
            sbQuery.AppendLine("	CAST(MV.mtv_pro_id AS VARCHAR(256)) = '" + req.ProfileID + "'");
            sbQuery.AppendLine("	AND MV.mtv_tim_date >= '" + req.StartDate.ToString("yyyy-MM-dd 00:00:00") + "'");
            sbQuery.AppendLine("	AND MV.mtv_tim_date <= '" + req.EndDate.ToString("yyyy-MM-dd 23:59:59.999") + "'");

            if (req.Metrics != null && req.Metrics.Count > 0)
            {
                sbQuery.AppendLine("	AND (");
                for (int i = 0; i < req.Metrics.Count; i++)
                {
                    if (i > 0)
                    {
                        sbQuery.AppendLine("	OR ");
                    }
                    sbQuery.AppendLine("	MV.mtv_tpt_id = '" + req.Metrics[i].Provider + "'");
                    sbQuery.AppendLine("	AND MV.mtv_mdf_id IN ('" + string.Join("','", req.Metrics[i].Metrics) + "')");
                }
                sbQuery.AppendLine("	)");
            }

            sbQuery.AppendLine("GROUP BY");
            switch (req.Aggregation)
            {
                case AggregationLevel.Yearly:
                    sbQuery.AppendLine("	T.tim_year,");
                    break;
                case AggregationLevel.Monthly:
                    sbQuery.AppendLine("	T.tim_month,");
                    break;
                case AggregationLevel.Daily:
                    sbQuery.AppendLine("	T.tim_date,");
                    break;
                case AggregationLevel.None:
                default:
                    sbQuery.AppendLine("	T.tim_date,");
                    break;
            }
            sbQuery.AppendLine("	MV.mtv_pro_id,");
            sbQuery.AppendLine("	TP.tpt_id,");
            sbQuery.AppendLine("	TP.tpt_name,");
            sbQuery.AppendLine("	MD.mdf_friendly_name");
            sbQuery.AppendLine("ORDER BY");
            sbQuery.AppendLine("	TP.tpt_name,");
            sbQuery.AppendLine("	MD.mdf_friendly_name,");
            switch (req.Aggregation)
            {
                case AggregationLevel.Yearly:
                    sbQuery.AppendLine("	T.tim_year");
                    break;
                case AggregationLevel.Monthly:
                    sbQuery.AppendLine("	T.tim_month");
                    break;
                case AggregationLevel.Daily:
                    sbQuery.AppendLine("	T.tim_date");
                    break;
                case AggregationLevel.None:
                default:
                    sbQuery.AppendLine("	T.tim_date");
                    break;
            }
            sbQuery.AppendLine(";");

            return sbQuery.ToString();
        }
    }
}