﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1.DataQuery
{
    public class MetricsRequest
    {
        public string ProfileID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public AggregationLevel Aggregation { get; set; }
        public List<ProviderMetric> Metrics { get; set; }
    }
}