﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1
{
    public class Metric
    {
        public string MetricID { get; set; }
        public string MetricName { get; set; }
        public string MetricFriendlyName { get; set; }
        public string MetricDescription { get; set; }
        public Summary Summary { get; set; }
        public List<MetricValue> MetricValues { get; set; }
    }
}