﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1
{
    public class Profile
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string DirectoryName { get; set; }
        public string BusinessName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string TollfreeNumber { get; set; }
        public string ProfileImage { get; set; }
        public string DirectoryImage { get; set; }
    }
}