﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1
{
    public class MetricValue
    {
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
    }
}