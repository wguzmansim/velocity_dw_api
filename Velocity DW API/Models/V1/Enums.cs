﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1
{
    public enum AggregationLevel
    {
        None,
        Yearly,
        Monthly,
        Daily
    }
}