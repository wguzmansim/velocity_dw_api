﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Velocity_DW_API.Models.V1
{
    public class ThirdPartyMetric
    {
        public string ThirdPartyProviderID { get; set; }
        public string ThirdPartyName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AggregationLevel Aggregation { get; set; }
        public List<Metric> Metrics { get; set; }
    }
}