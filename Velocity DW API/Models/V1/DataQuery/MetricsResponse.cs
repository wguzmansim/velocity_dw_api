﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1.DataQuery
{
    public class MetricsResponse
    {
        public Profile Profile { get; set; }
        public List<ThirdPartyMetric> ThirdPartyMetrics { get; set; }
        public Status Status { get; set; }
    }
}