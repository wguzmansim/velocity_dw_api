﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Velocity_DW_API.Models.V1;
using Velocity_DW_API.Models.V1.DataQuery;

namespace Velocity_DW_API.Controllers.V1
{

    public class TPMController : ApiController
    {
        /// <summary>
        /// Retrieves the headers allowed by the server
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            HttpResponseMessage objHttpResponseMessage = new HttpResponseMessage();
            objHttpResponseMessage.StatusCode = HttpStatusCode.OK;
            return objHttpResponseMessage;
        }


        [HttpPost]
        [Route("api/TPM/V1/GetMetrics")]
        public MetricsResponse GetMetrics(MetricsRequest Req)
        {
            string Token;
            try
            {
                Token = Request.Headers.GetValues("Token").FirstOrDefault();
            }
            catch (Exception)
            {
                return new MetricsResponse() { Status = Status.NoToken() };
            }

            return new BL.V1.TPM().GetMetrics(Req, Token);
        }
    }
}
