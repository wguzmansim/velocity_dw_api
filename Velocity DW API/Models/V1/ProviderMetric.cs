﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Velocity_DW_API.Models.V1
{
    public class ProviderMetric
    {
        public string Provider { get; set; }
        public List<string> Metrics { get; set; }
    }
}